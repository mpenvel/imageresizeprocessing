package uma.muii;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;

import java.io.*;
import java.util.List;

/**
 * Created by Antonio on 15/11/2016.
 */
public class BucketUtil {
    private AmazonS3 s3client;
    private AmazonSQS sqs;
    public static final String MY_URL = "https://sqs.eu-central-1.amazonaws.com/832723294680/QueueAngelManuProcess";
    public static final String URL_SENDER = "https://sqs.eu-central-1.amazonaws.com/832723294680/QueueAngelManuUpload";


    public BucketUtil() {
        AWSCredentials awsc;
        EnvironmentVariableCredentialsProvider evcp = new EnvironmentVariableCredentialsProvider();
        awsc = new ProfileCredentialsProvider("default").getCredentials();
        s3client = new AmazonS3Client(awsc);
        s3client.setRegion(Region.getRegion(Regions.EU_WEST_1));
        sqs = new AmazonSQSClient(new ProfileCredentialsProvider());
        sqs.setRegion(Region.getRegion(Regions.EU_WEST_1));
    }

    public void createBucket(String bucketName) {
        try {
            if (!s3client.doesBucketExist(bucketName)) {
                s3client.createBucket(bucketName);
            }
        } catch (AmazonServiceException ase) {
            printAmazonServiceException(ase);
        } catch (AmazonClientException ace) {
            printAmazonClientException(ace);
        }

    }

    public String uploadObject(String uploadFileName, String bucketName, String keyName) {
        String url = "http://" + bucketName + ".s3.amazonaws.com/" + keyName;
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            File file = new File(uploadFileName);
            PutObjectRequest por = new PutObjectRequest(
                    bucketName, keyName, file);
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3client.putObject(por);
        } catch (AmazonServiceException ase) {
            printAmazonServiceException(ase);
        } catch (AmazonClientException ace) {
            printAmazonClientException(ace);
        }

        return url;
    }

    public List<Message> receiveMessage() {
        ReceiveMessageRequest request = new ReceiveMessageRequest().withQueueUrl(MY_URL)
                .withWaitTimeSeconds(1).withMaxNumberOfMessages(10);
        ReceiveMessageResult result = sqs.receiveMessage(request);
        List<Message> messages = result.getMessages();
        boolean messagesReceived = messages.size() > 0;

        if (messagesReceived) {
            return messages;
        } else {
            return null;
        }
    }

    public File getObjectFromBucket(String bucketName, String fileName) throws IOException {
        ObjectListing objectListing;
        PrintWriter writer;
        S3Object s3Object;
        BufferedReader reader;
        String name = "";
        do {
            objectListing = s3client.listObjects(new
                    ListObjectsRequest().withBucketName(bucketName));

            for (S3ObjectSummary object : objectListing.getObjectSummaries()) {
                s3Object = s3client.getObject(new GetObjectRequest(bucketName, object.getKey()));
                name = s3Object.getKey();
                if (name.equals(fileName)) {
                    writer = new PrintWriter(s3Object.getKey(), "UTF-8");
                    reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        writer.println(line);
                    }
                    writer.close();
                    reader.close();
                }
            }
        } while (objectListing.isTruncated());

        return new File(name);
    }

    public void deleteMessage(List<Message> messages) {
        System.out.println("Deleting a message.\n");
        String messageReceiptHandle = messages.get(0).getReceiptHandle();
        sqs.deleteMessage(new DeleteMessageRequest(MY_URL, messageReceiptHandle));
    }

    public void sendMessage(String message) {
        System.out.println("Sending a message to MyQueue.\n");
        sqs.sendMessage(new SendMessageRequest(URL_SENDER, message));
    }


    private void printAmazonClientException(AmazonClientException ace) {
        System.out.println("Caught an AmazonClientException, which " +
                "means the client encountered " +
                "an internal error while trying to " +
                "communicate with S3, " +
                "such as not being able to access the network.");
        System.out.println("Error Message: " + ace.getMessage());
    }

    private void printAmazonServiceException(AmazonServiceException ase) {
        System.out.println("Caught an AmazonServiceException, which " +
                "means your request made it " +
                "to Amazon S3, but was rejected with an error response" +
                " for some reason.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());
    }


}

