/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uma.muii;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.amazonaws.services.sqs.model.Message;

/**
 * @author Antonio
 */

public class ImageResize {

    private static BucketUtil bucket;
    private static String bucketName;
    private static String url;

    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("Empiezo");
        bucket = new BucketUtil();
        Object monitor = new Object();
        int visibilityTimeout = 10;
        while (true) {
            System.out.println("YEAHH");
            synchronized (monitor) {
                try {
                    monitor.wait(visibilityTimeout * 1000);
                } catch (InterruptedException e) {

                }
            }

            boolean messagesReceived = false;
            do {
                int random = (int) Math.abs(Math.random() * 100000);
                List<Message> messages = bucket.receiveMessage();
                messagesReceived = (messages != null);
                if (!messagesReceived) {
                    break;
                }

                System.out.println("Back-end recibo: " + messages.get(0).getBody());
                String[] partsOfMessage = messages.get(0).getBody().split("\t");
                String bucketName = partsOfMessage[0];
                String filename = partsOfMessage[1];
                Integer width = Integer.parseInt(partsOfMessage[2]);
                Integer height = Integer.parseInt(partsOfMessage[3]);

                try {
                    System.out.println("Descarga imagen original");
                    File file = bucket.getObjectFromBucket(bucketName, filename);
                    System.out.println("Descarga Terminada. Comienza conversion");

                    String[] cmd = {"./NConvert/nconvert", "-out", "jpeg", "-resize", String.valueOf(height),
                            String.valueOf(width), "-o", "IMG_" + random + "new.jpg", file.getName()};
                    Runtime.getRuntime().exec(cmd);
                    System.out.println("Termina conversion. Sube fichero");

                    url = bucket.uploadObject("IMG_" + random + "new.jpg", bucketName, file.getName());
                    System.out.println("Objeto subido.");
                    File f = new File("IMG_" + random + "new.jpg");
                    f.delete();
                    f = new File(file.getName());
                    f.delete();

                    bucket.deleteMessage(messages);
                    bucket.sendMessage(url);
                    System.out.println("Back-end envio: " + url);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } while (messagesReceived);
        }
    }
}
